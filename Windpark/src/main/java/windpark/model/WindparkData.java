package windpark.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@XmlRootElement(name = "WindparkData")
public class WindparkData {

    @XmlElement(name = "WindengineData")
    private WindengineData[] windpark;

    public WindparkData(WindengineData[] windpark) {
        this.windpark = windpark;
    }


    public WindengineData[] getData() {
        return windpark;
    }
}
