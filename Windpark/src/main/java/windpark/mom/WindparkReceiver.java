package windpark.mom;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import windpark.model.WindengineData;
import windpark.model.WindengineMessage;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Wird nicht verwendet, alle Funktionalität wurde in WindparkController übertragen
 */

@Component
public class WindparkReceiver {

    /**
    public ConcurrentHashMap<String, LinkedList<WindengineData>> data;

    private ObjectMapper mapper;

    public WindparkReceiver() {
        data = new ConcurrentHashMap<>();
        mapper = new ObjectMapper();
        AnnotationIntrospector introspector = new JaxbAnnotationIntrospector(mapper.getTypeFactory());
        mapper.setAnnotationIntrospector(introspector);
    }


    @JmsListener(destination = "windpark", containerFactory = "myFactory")
    public void receiveMessage(WindengineMessage message) {
        WindengineData wd = null;
        try {
            wd = mapper.readValue(message.getMessage(), WindengineData.class);
            if(data.containsKey(wd.getWindengineID())) {
                LinkedList<WindengineData> list = data.get(wd.getWindengineID());
                list.add(wd);
            }
            else {
                LinkedList<WindengineData> list = new LinkedList<>();
                list.add(wd);
                data.put(wd.getWindengineID(), list);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }




        System.out.println("Received <" + message + ">");
        if(wd != null) System.out.println(wd.getWindengineID());
    }
    */
}
	
