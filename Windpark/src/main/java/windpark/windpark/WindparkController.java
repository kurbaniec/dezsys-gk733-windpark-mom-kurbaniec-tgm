package windpark.windpark;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import windpark.model.WindengineData;
import windpark.model.WindengineMessage;
import windpark.model.WindparkData;
import windpark.mom.WindparkReceiver;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@RestController
public class WindparkController {
    @Autowired
    JmsTemplate jmsListener;


    @RequestMapping(value = "/windpark/data_xml", produces = MediaType.APPLICATION_XML_VALUE)
    public WindparkData windparkDataXML() {
        ArrayList<WindengineData> elements = new ArrayList<>();
        for(String key: data.keySet()) {
            LinkedList<WindengineData> list = data.get(key);
            elements.add(list.getLast());
        }
        WindengineData[] show = elements.toArray(new WindengineData[0]);
        return new WindparkData(show);
    }

    // Gespeicherte Daten als JSON ausgeben
    @RequestMapping(value = "/windpark/data_json", produces = MediaType.APPLICATION_JSON_VALUE)
    public WindparkData windparkDataJSON() {
        ArrayList<WindengineData> elements = new ArrayList<>();
        for(String key: data.keySet()) {
            LinkedList<WindengineData> list = data.get(key);
            elements.add(list.getLast());
        }
        WindengineData[] show = elements.toArray(new WindengineData[0]);
        return new WindparkData(show);
    }


     public ConcurrentHashMap<String, LinkedList<WindengineData>> data;

     private ObjectMapper mapper;

     public WindparkController() {
        data = new ConcurrentHashMap<>();
        mapper = new ObjectMapper();
        AnnotationIntrospector introspector = new JaxbAnnotationIntrospector(mapper.getTypeFactory());
        mapper.setAnnotationIntrospector(introspector);
     }

    // Empfangene Daten korrekt verarbeiten
     @JmsListener(destination = "windpark", containerFactory = "myFactory")
     public void receiveMessage(WindengineMessage message) {
         WindengineData wd = null;
         try {
             wd = mapper.readValue(message.getMessage(), WindengineData.class);
             Timestamp timestamp = new Timestamp(System.currentTimeMillis());
             jmsListener.convertAndSend("windengine"+wd.getWindengineID(), new WindengineMessage( timestamp + " Data successfully transferred", true));
             if(data.containsKey(wd.getWindengineID())) {
                 LinkedList<WindengineData> list = data.get(wd.getWindengineID());
                 list.add(wd);
             }
             else {
                 LinkedList<WindengineData> list = new LinkedList<>();
                 list.add(wd);
                 data.put(wd.getWindengineID(), list);
            }
         }
         catch (Exception ex) {
         System.out.println(ex.getMessage());
     }


     System.out.println("Received <" + message + ">");
     if(wd != null) System.out.println(wd.getWindengineID());
     }

}
