$(document).ready(function() {
    $.ajax({
        url: "http://localhost:8080/windengine/001/data_json"
    }).then(function(data) {
        $('.windengineID').append(data.windengineID);
        $('.timestamp').append(data.timestamp);
        $('.windspeed').append(data.windspeed);
        $('.unitWindspeed').append(data.unitWindspeed);
        $('.temperature').append(data.temperature);
        $('.unitTemperature').append(data.unitTemperature);
        $('.power').append(data.power);
        $('.unitPower').append(data.unitPower);
        $('.blindpower').append(data.blindpower);
        $('.unitBlindpower').append(data.unitBlindpower);
        $('.rotationspeed').append(data.rotationspeed);
        $('.unitRotationspeed').append(data.unitRotationspeed);
        $('.bladeposition').append(data.bladeposition);
        $('.unitBladeposition').append(data.unitBladeposition);
    });
});