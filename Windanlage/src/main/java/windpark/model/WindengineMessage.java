package windpark.model;

public class WindengineMessage {
    private String id;

    private String message;

    private boolean park;

    public WindengineMessage() {
    }

    public WindengineMessage(String message) {
        this.message = message;
    }

    public WindengineMessage(String message, String id) {
        this.id = id;
    }

    public WindengineMessage(String message, boolean park) {
        this.message = message;
        this.park = park;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getPark() {
        return park;
    }

    @Override
    public String toString() {
        if(park) {
            return String.format("windpark: %s", getMessage());
        }
        else return String.format("data{message=%s}", getMessage());
    }

}