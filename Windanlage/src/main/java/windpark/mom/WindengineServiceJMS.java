package windpark.mom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import windpark.windengine.WindengineService;

import javax.annotation.PostConstruct;

/**
 * @author Kacper Urbaniec
 * @version 17.01.2019
 */
@Service
@Configurable
public class WindengineServiceJMS {
    @Autowired
    ApplicationContext context;

    @Autowired
    TaskExecutor taskExecutor;



    @PostConstruct
    public void executeAsynchronously() {
        System.out.println("Starting sending");
        // Neuen Sende-Thread erstellen
        WindengineSender sender = context.getBean(WindengineSender.class);
        // Thread ausführen
        taskExecutor.execute(sender);
    }
}

