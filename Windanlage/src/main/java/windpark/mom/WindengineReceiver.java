package windpark.mom;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import windpark.model.WindengineMessage;

@Component
public class WindengineReceiver {

    // Empfangene Nachrichten ausgeben
    @JmsListener(destination = "windengine${windengine.id}", containerFactory = "myFactory")
    public void receiveMessage(WindengineMessage message) {
        System.out.println("Received <" + message + ">");
    }

}

