package windpark.mom;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import windpark.Gk72Application;
import windpark.model.WindengineData;
import windpark.model.WindengineMessage;
import windpark.windengine.WindengineService;



/**
 * Sendet alle drei Sekunden Daten an Windpark-Zentrale
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@Component
@Scope("prototype")
public class WindengineSender implements Runnable{
    @Autowired
    private ApplicationContext context;

    @Autowired
    private WindengineService service;

    private JmsTemplate jmsTemplate;

    private boolean sending =  true;

    private WindengineData data;

    private ObjectMapper mapper;

    @Value("${windengine.id}")
    private String id;

    public WindengineSender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
        // Erzeugt Jackson JSON-Converter
        mapper = new ObjectMapper();
        AnnotationIntrospector introspector = new JaxbAnnotationIntrospector(mapper.getTypeFactory());
        mapper.setAnnotationIntrospector(introspector);

    }

    @Override
    public void run() {
        while (sending) {
            try {
                jmsTemplate.convertAndSend("windpark", mapper.writeValueAsString(service.getWindengineData(id)));
                Thread.sleep(3000);
            }
            catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void shutdown() {
        sending = false;
    }
}
