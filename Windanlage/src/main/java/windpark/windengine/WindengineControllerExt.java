package windpark.windengine;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Da der WindengineController ein RestController ist, gibt dieser alle Objekte
 * als XML/JSON aus. Um daher HTML-Websiten und mehr zurückzugeben, habe ich diesen
 * Controller erstellt.
 * @author Kacper Urbaniec
 * @version 30.09.2018
 */

@Controller
public class WindengineControllerExt {

    /**
     * Methode wird benötigt um die Website "Consumer.html" zurückzugeben,
     * die eine Tabelle bildet
     * @return Consumer-Website
     */

    //@CrossOrigin(origins = "*")
    @RequestMapping(value = "/consumer")
    public String consumer() {
        return "consumer";
    }

    @RequestMapping(value = "/")
    public String main() {
        return "main";
    }



}
