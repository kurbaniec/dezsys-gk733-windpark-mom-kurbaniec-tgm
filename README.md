# DEZSYS_GK733_WINDPARK_MOM

## Einführung

Diese Übung soll die Funktionsweise und Implementierung von eine Message Oriented Middleware (MOM) mit Hilfe des **Frameworks Apache Active MQ** demonstrieren. **Message Oriented Middleware (MOM)** ist neben InterProcessCommunication (IPC), Remote Objects (RMI) und Remote Procedure Call (RPC) eine weitere Möglichkeit um eine Kommunikation zwischen mehreren Rechnern umzusetzen.

Die Umsetzung basiert auf einem praxisnahen Beispiel einer Windkraftanalage. Ein Windkraftanalage (Windrad) ist immer Teil eines Verbunds, genannt Windpark. Jede Windkraftanlage beinhaltet einen Rechner, der die Daten der Windkraftanalage aufzeichnet und diese steuern kann. Die Daten werden als REST Schnittstelle in XML oder JSON zur Verfügung gestellt. Die Daten aller Windkraftanlagen eines Windparks werden von einem Parkrechner gesammelt und abgespeichert. Der Parkrechner kommuniziert mit dem Rechenzentrum in der Zentrale. Eine Zentrale kommuniziert mit mehreren Windparks und steuert diese.

![Windpark Architektur](https://elearning.tgm.ac.at/draftfile.php/2668/user/draft/584681179/dezsys_windpark.png)

## 1.1 Ziele  

Das Ziel dieser Übung ist die **Implementierung einer Kommunikationsplattform für einen Windpark. Dabei erfolgt ein Datenaustausch von mehreren Windkraftanlage mit dem Parkrechner unter Verwendung einer Message Oriented Middleware (MOM)**. Die einzelnen Daten der Windkraftanlage sollen an den Parkrechner ü<span>bertragen werden</span>. Es sollen **nachrichtenbasierten Protokolle mit Message Queues** verwendet werden. Durch diese lose Kopplung kann gewährleistet werden, dass in Zukunft weitere Anlagen hinzugefügt bzw. Kooperationspartner eingebunden werden können.

Neben der REST-Schnittstelle muss nun eine Schnittstelle bei der Windkraftanlage implementiert werden, die in regelmässigen Abständen die Daten von der REST-Schnittstelle ausliest und diese in eine Message Queue von **Apache Active MQ** hinzufügt. Um die Datenintegrität zu garantieren, sollen jene Daten, die mit der Middleware übertragen werden in einer LOG-Datei abgespeichert werden.  

## 1.2 Voraussetzungen

*   Grundlagen Architektur von verteilten Systemen
*   Grundlagen zur nachrichtenbasierten Systemen / Message Oriented Middleware  

*   Verwendung des Message Brokers Apache ActiveMQ
*   Verwendung der XML- oder JSON Datenstruktur der Windkraftanlage (siehe GK7.3.2)  

*   Verwendung der JMSChat.jar JAVA Applikation als Startpunkt für diese Aufgabenstellung  

## 1.3 Aufgabenstellung

Implementieren Sie die Windpark-Kommunikationsplattform mit Hilfe des Java Message Service. Verwenden Sie Apache ActiveMQ ([http://activemq.apache.org](http://activemq.apache.org/)) als Message Broker Ihrer Applikation. Das Programm soll folgende Funktionen beinhalten:

 *   Installation von Apache ActiveMQ am Parkrechner.
 *   Jede Windkraftanlage erstellt eine Message Queue mit einer ID am Parkrechner.
 *   Jede Windkraftanlage legt in regelmässigen Abständen die Daten der Anlage in der Message Queue ab.
 *   Bei einer erfolgreichen Übertragung empfängt die Windkraftanlage die Nachricht "SUCCESS".
 *   Der Parkrechner fragt in regelmässigen Abständen alle Message Queues ab.
 *   Der Parkrechner <span>f<span style="">ügt alle Daten aller Windkraftanlagen zusammen und stellt diese an einer REST Schnittstelle im JSON/XML Format zur Verfügung.  

## 1.4 Bewertung  

 *   Gruppengrösse: 1 Person  
 *   Anforderungen **"überwiegend erfüllt"**

	 *   Implementierung der Kommunikation zwischen einer Windkraftanlage und dem Parkrechner (JMS Queue)  

	 *   Implementierung der REST Schnittstelle am Parkrechner
		*   Zusammensetzung der Daten aller Windkraftanlagen in eine zentrale JSON/XML-Struktur

 *   Anforderungen **"zur Gänze erfüllt"**

	 *   Implementierung der Kommunikation mit mehreren Windkraftanlage und dem Parkrechner  

	 *   Rückmeldung des Ergebnisses der Übertragung vom Parkrechner an die Windkraftanlage (JMS: Topic)  

	 *   LOG-Datei bei jeder Windkraftanlage
	 *   LOG-Datei für den Parkrechner mit allen eingehenden Daten aller Windkraftanlagen

## 1.5 Fragestellung für Protokoll

 *   Nennen Sie mindestens 4 Eigenschaften der Message Oriented Middleware?  

 *   Was versteht man unter einer transienten und synchronen Kommunikation?
 *   Beschreiben Sie die Funktionsweise einer JMS Queue?
 *   JMS Overview - Beschreiben Sie die wichtigsten JMS Klassen und deren Zusammenhang?
 *   Beschreiben Sie die Funktionsweise eines JMS Topic?
 *   Was versteht man unter einem lose gekoppelten verteilten System? Nennen Sie ein Beispiel dazu. Warum spricht man hier von lose?

## 1.6 Links & Dokumente

 *   Grundlagen Message Oriented Middleware: [Präsentation](https://elearning.tgm.ac.at/pluginfile.php/84683/mod_resource/content/2/dezsys_mom.pdf)
 *   Middleware: [Apache ActiveMQ Installationspaket](http://activemq.apache.org/activemq-5153-release.html)
 *   Apache ActiveMQ & JMS Tutorial:
		*   http://activemq.apache.org/index.html
		*   http://www.academictutorials.com/jms/jms-introduction.asp
		*   http://docs.oracle.com/javaee/1.4/tutorial/doc/JMS.html#wp84181
		*   http://www.oracle.com/technetwork/systems/middleware/jms-basics-jsp-135286.html
		*   http://www.oracle.com/technetwork/articles/java/introjms-1577110.html
		*   https://spring.io/guides/gs/messaging-jms/
		*   https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-messaging.html
		*   https://dzone.com/articles/using-jms-in-spring-boot-1

## Durchführung
Ordnerstruktur erstellt:

* Windanlage -> einzelne Windanlage
* Windpark -> Zentrale für die Windanlagen

Windanlage zum Testen auf `server.port = 8090` für Spring gestellt

Als nächstes habe ich ApacheMQ von [hier](http://activemq.apache.org/activemq-5153-release.html) geholt. Um es zu
"installieren" muss man es nur in einem Ordner entzippen und in einer Command-Line bzw. Terminal starten. Dazu navigiert
man zum Ordner und führt `bin\activemq start` aus.

Damit die Spring-Servlets mit Apache MQ, genauer dem Broker, arbeiten, muss man ApacheMQ in den
`application.properties` definieren:
```
spring.activemq.broker-url=tcp://localhost:61616
spring.activemq.user=admin
spring.activemq.password=admin
```
Die erste Anweisung mapped den Broker mit einer Adresse, an die die Implemtierung von Spring für MQ Messages senden
bzw. oder von ihr empfangen kann. Die nächsten Zeile sind nur die für Authentifizierung zuständing, da muss nur die Default-Anmelde Daten angeben.

Beim Starten von ApacheMQ in der CMD stand eine interessaten Zeile:  
ActiveMQ WebConsole available at http://0.0.0.0:8161/

Wenn man diese Adresse im Browser öffnet kommt nichts, doch wenn man statdessen localhost:8161 angibt, kommt man zur grafischen Oberläche für ApacheMQ. Dort
meldet man sich mit dem Standard-User (admin, admin) an und bekommt einen Überblick über den Zustand der Queue.

Als nächstes habe ich die für die Implementierung der Message-Oriented-Middleware die vorhanden Klassen dafür vom Windpark meiner Windanlage hinzugefügt und adaptiert. Nach kurzer Dependency-Scherberei
konnte ich zwischen den beiden die ersten Nachrichten senden:
![Windrechner](img/message_1.PNG)
Windpark sendet "Hallo Spencer", empfangt "Hallo Spencer2" von Windparkanlage
![Windrechner](img/message_2.PNG)
Windpark sendet "Hallo Spencer2", empfangt "Hallo Spencer" von Windparkanlage

Diese Messages werden auch in der grafischen Oberfläche angezeigt:
![GUI](img/gui.PNG)

### Mein Konzept
In meinen Windanlagen habe ich Properties für die ID der Anlage und für den Serverport. Beim Starten werden dieses als Parameter mitgegeben. Beim Start wird ein Service (`WindengineServiceJMS`) gestartet, der einen Executor erzeugt, der einen Thread startet (`WindengineSender`). Dieser Thread ist für das Senden der Daten an die Windpark-Zentrale zuständig. Das Senden findet in einem Intervall von drei Sekunden statt. Gesendet wird eine `WindengineMessage`, die die Daten der Windengine (`WindengineData`) in einem Sring als JSON serialisiert sendet. Theoretisch hätte man nur `WindengineData` zum Senden verwenden können, doch dadurch kann ich in `WindengineMessage` zusätzliche Funktionen implementieren, dadurch ist das System in Zukunft erweiterbar.

Die Windpark-Zentrale hat einen Rest-Controller (`WindparkController`), der aber auch eine Methode zum Empfangen der JMS-Nachrichten hat. Diese empfängt die Daten als `WindengineMessage`, aus diesen wird auch die `WindengineData` geholt. Diese Daten werden in einer Liste gespeichert, die alle Daten einer Windanlage speichert, die in einer Map wiederum hinzugefügt wird, die alle Windanlagen speichert. Beim erfolgreichen Empfang der Daten wird eine Meldung an die entsprechende Windanlage gesendet. Die Daten können per REST auf der Adresse (`/windpark/data_json`) der Windpark-Zentrale angezeigt werden.
![REST](img/rest.PNG)

### Starten der Anwendung
Im Projekt gibt es zwei Hauptordner:
* Windanlage
* Windpark

#### Windanlage
Spring-Applet, dass eine Windanlage simuliert. Einfach die Hauptklasse `Gk72Application` ausführen. Für jede Anlage sollte ein eigener Serverport und eine einzigartige ID gewählt werden, sonst läuft standardmäßig Serverport 8090 und windengine.id = 0.
![Windanlage](img/windanlageConf.PNG)

#### Windpark
Spring-Applet, dass die Windpark-Zentrale repräsentiert. Einfach die Hauptklasse `Gk73Application` ausführen. Grundsätzlich passen alle Parameter, der Server läuft auf Port 8080.
![Windpark](img/windparkConf.PNG)

## Fragestellung

 *   Nennen Sie mindestens 4 Eigenschaften der Message Oriented Middleware?
     1. Für lose gekoppelten Architekturen geeignet
     2. Erlaubt asynchrone Kommunikation und Realisierung von Warteschlangen
     3. Server und Client müssen nicht sofort verfügbar sein
     4. Parallele Verarbeitung von Nachrichten möglich
 *   Was versteht man unter einer transienten und synchronen Kommunikation?  
     Transiente Kommunikation: Nachrichten werden so lange gespeichert, solange ein Sender und Empfänger arbeiten.  
     Synchrone Kommunikation: Der Sender ist so lange blockiert, bis Nachricht beim Empfänger eingereicht und angekommen ist und von ihm verarbeitet wurde.
 *   Beschreiben Sie die Funktionsweise einer JMS Queue?  
     Es gibt eine undefinierte Anzahl von Sendern und Empfängern, wo jeder Sender genau einem Empfänger die Nachricht sendet (point-to-point) Verbindung
 *   JMS Overview - Beschreiben Sie die wichtigsten JMS Klassen und deren Zusammenhang?  
     **ConnectionFactory:** Ein ConnectionFactory Objekt kapselt verschiedenste Konfigurations-Parameter, die von einem Administrator definiert worden sind. Der Client verwendet diese um sich mit dem JMS-Anbieter (Provider) zu verbinden.  
     **Connection:** Repräsentiert ein Objekt des Clients über die aktive Verbindung zum JMS-Anbieter. Normalerweise verschiebt es Ressourcen außerhalb der JVM.  
     **Session:** Ist ein Objekt, dass single-threaded arbeitet. Es dient dazu da, um Nachrichten zu versenden oder empfangen.  
     **Message:** Interface für alle JMS-Messages, somit gibt es die Struktur aller JMS-Messages vor (Methoden, etc.).   
     **MessageProducer:** Ein Client benutzt ein MessageProducer-Objekt um Daten zum Zielort zu senden. Benötigt ein Destination-Objekt.  
     **MessageConsumer:** Ein Client benutzt ein MessageConsumer-Objekt um Daten von einem Ursprungsort zu empfangen. Ursprungsort ist ein Destination-Objekt.  
     **Destination:** Kapselt eine Provider-spezifische Adresse in ein Objekt. Eine wirkliche Standard-Adresse gibt es nicht.
 *   Beschreiben Sie die Funktionsweise eines JMS Topic?  
     Es gibt eine undefinierte Anzahl von Sendern und Empfängern, wo jeder Sender jedem ihm zugeschriebenen Empfänger (Subscriber) eine Nachricht sendet. Wird als Publisher Prinzip bezeichnet.
 *   Was versteht man unter einem lose gekoppelten verteilten System?    
  Nennen Sie ein Beispiel dazu. Warum spricht man hier von lose?  
     Bezeichnet das Computer oder Komponenten in einem verteilten System untereinander lose gekoppelt sind, also keine große Abhängigkeit Zwischen ihnen besteht. Dadurch können die einzelnen Computer leichter durch andere ersetzt werden.
     Wenn wir dieses Prinzip auf ApacheMQ anwenden, so kann man zum Beispiel sagen, dass man mehrere Systeme macht, aber diese mit verschiedenen Frameworks und Programmiersprachen umsetzt. Es gibt keine Pflicht, dass wenn eine Maschine diese Sprache verwendet, dass die andere die gleiche benutzen muss. Der Grund dafür ist, dass ApacheMQ wie viele andere MOM-Systeme Cross-Language Support besitzt, dass heißt es gibt dafür geeignete APIs für verschiedene Programmiersprachen. Somit kann man die einzelnen Systeme mit der Sprache umsetzen, die am besten den Zweck erfüllt. Die Kommunikation erfüllt nicht-sprachgebunden per MOM.


## Quellen
http://activemq.apache.org/activemq-5153-release.html, aufgerufen am 15.01.2019

http://activemq.apache.org/getting-started.html#GettingStarted-StartingActiveMQStartingActiveMQ, aufgerufen am 15.01.2019

Credentials: https://stackoverflow.com/questions/21705899/how-to-change-password-admin-user-activemq-5-9-new-web-console, aufgerufen am 15.01.2019

Port: http://activemq.apache.org/getting-started.html#GettingStarted-Listenport, aufgerufen am 15.01.2019

Thread: https://dzone.com/articles/spring-and-threads-taskexecutor

Dynamic Port: https://stackoverflow.com/questions/49431869/thymeleaf-external-javascript-file-shares-the-module-attributes-with-html-file,
https://stackoverflow.com/questions/21726119/how-to-access-system-properties-in-thymeleaf-template, aufgerufen am 17.01.2019

Starting service: https://stackoverflow.com/questions/26353684/starting-and-stopping-a-service-instance-in-spring-mvc, aufgerufen am 21.01.2019

JMS: https://docs.oracle.com/javaee/6/tutorial/doc/bncdr.html, aufgerugen am 22.01.2019

Distributed Systems Technologies
(Technologien für Verteilte Systeme), Lorenz Froihofer, http://www.infosys.tuwien.ac.at/teaching/courses/dst/, aufgerufen am 22.01.2019
